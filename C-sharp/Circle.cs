﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_sharp
{

    public class Circle
    {
        private double radius;
        private string color;

        public Circle()
        {
            //Oletusarvot
            this.radius = 1;
            this.color = "red";
        }
        public Circle(double r)
        {
            //mainissa annetut arvot
            this.radius = r;
            this.color = "red";
        }
        public double getRadius()
        {
            return radius;
        }

        // A public method for computing the area of circle
        public double getArea()
        {
            return radius * radius * Math.PI;
        }
        public Circle(double r, String color)
        {

            this.radius = r;
            this.color = color;

        }
        public string getColor()
        {
            return color;
        }
        public void setRadius(double newRadius)
        {
            radius = newRadius;
        }
        public void setColor(String newColor) {
            color = newColor;
        }
    }
}
