﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_sharp
{
    class Program
    {
        static void Main(string[] args)
        {

            Circle c1 = new Circle();


            Console.WriteLine("The circle has radius of "
                + c1.getRadius() + " and area of " + c1.getArea());


            Circle c2 = new Circle(5.0);

            Console.WriteLine("The circle has radius of "
                   + c2.getRadius() + " and area of " + c2.getArea());

        
            //c3 antaa ympyrälle säteen ja värin.
            Circle c3 = new Circle(3.0, "Sininen");
            Console.WriteLine("Ympyrän säde on " + c3.getRadius() + ", sen pinta-ala on "
                + c3.getArea() + " ja väri on " + c3.getColor());

            Circle c4 = new Circle();   // construct an instance of Circle
            c4.setRadius(5.0);          // change radius
            Console.WriteLine("radius is: " + c4.getRadius()); // Print radius via getter
            c4.setColor("Vihreä");        // Change color
            Console.WriteLine("color is: " + c4.getColor());   // Print color via getter

            // You cannot do the following because setRadius() returns void,
            // which cannot be printed.
            Console.WriteLine (c4.setRadius(4.0));

        }
    }
}

    

    
    
         
