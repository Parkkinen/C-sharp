﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassTest
{
    public class Circle
    {
        //Luodaan luokan sisäiset muuttujat
        private double radius;
        private String color;

        //Asetetaan muuttujien oletusarvot
        public Circle()
        {
            radius = 1.0;
            color = "red";
        }

        // 2nd constructor with given radius, but color default
        public Circle(double r)
        {
            radius = r;
            color = "red";
        }

        // public method jolla voidaan hakea muuttujan radius arvo
        public double getRadius()
        {
            return radius;
        }

        // public method jolla lasketaan ympyrän pinta-ala
        public double getArea()
        {
            return radius * radius * Math.PI;
        }

    }
}
