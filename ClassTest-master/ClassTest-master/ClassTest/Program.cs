﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Määritetään uusi Circle-luokan instanssi c1, 
            // joka käyttää luokan oletusarvoja
            Circle c1 = new Circle();

            // Tulostetaan arvot
            Console.WriteLine("Ympyrän säde on "
               + c1.getRadius() + " ja sen pinta-ala on " + c1.getArea());

            // Määritetään uusi Circle-luokan instanssi c2.
            // c2 antaa ympyrälle uuden säteen, mutta käyttää värinä oletusarvoa
            Circle c2 = new Circle(2.0);
            // Tulostetaan arvot
            Console.WriteLine("Ympyrän säde on "
               + c2.getRadius() + " ja sen pinta-ala on " + c2.getArea());


        }
    }
}
