﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ympyrä
{
    public class Circle
    {
        //Luodaan luokan sisäiset muuttujat
        private double radius;
        private String color;

        //Asetetaan muuttujien oletusarvot
        public Circle()
        {
            radius = 1.0;
            color = "red";
        }

        // toinen constructor jolle annetaan säde, mutta väri on oletus
        public Circle(double radius)
        {
            this.radius = radius;
            color = "red";
        }

        //Kolmas constructor jolle annetaan säde ja väri
        public Circle(double radius, string color)
        {
            this.radius = radius;
            this.color = color;
        }

        // public method jolla voidaan hakea muuttujan radius arvo
        public double getRadius()
        {
            return radius;
        }

        public string getColor()
        {
            return color;
        }

        // public method jolla lasketaan ympyrän pinta-ala
        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        // Setter for instance variable radius
        public void setRadius(double radius)
        {
            this.radius = radius;
        }

        // Setter for instance variable color
        public void setColor(String color)
        {
            this.color = color;
        }

        // Tulostaa tiedot alla olevassa muodossa
        // Circle[radius=r,color=c]
        public override String ToString()
        {
            return "Circle[radius=" + radius + " color=" + color + "]";
        }
    }
}
