﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ympyrä
        
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Määritetään uusi Circle-luokan instanssi c1, 
            // joka käyttää luokan oletusarvoja
            Circle c1 = new Circle();


            // Tulostetaan arvot
            Console.WriteLine("Ympyrän säde on "
               + c1.getRadius() + " ja sen pinta-ala on " + c1.getArea());

            Console.WriteLine("----"); //erotin

            // Määritetään uusi Circle-luokan instanssi c2.
            // c2 antaa ympyrälle uuden säteen, mutta käyttää värinä oletusarvoa
            Circle c2 = new Circle(2.0);
            // Tulostetaan arvot
            Console.WriteLine("Ympyrän säde on "
               + c2.getRadius() + " ja sen pinta-ala on " + c2.getArea());

            Console.WriteLine("----"); //erotin

            // Määritetään Circle-luokan instanssi c3
            // c3 antaa ympyrälle säteen ja värin.
            Circle c3 = new Circle(3.0, "blue");
            Console.WriteLine("Ympyrän säde on " + c3.getRadius() + ", sen pinta-ala on " 
                + c3.getArea() + " ja väri on " + c3.getColor());

            Console.WriteLine("----"); //erotin

            //Määritetään Circle-luokan instanssi c4
            Circle c4 = new Circle();
            c4.setRadius(5.0); // asetetaan säteeksi 5
            Console.WriteLine("Ympyrän säde on " + c4.getRadius()); //Tulostetaan teksti ja arvo
            c4.setColor("pinkki"); //asetetaan väriksi pinkki
            Console.WriteLine("Ympyrän väri on " + c4.getColor()); //Tulostetaan teksti ja arvo

            // You cannot do the following because setRadius() returns void,
            // which cannot be printed.
            // Console.WriteLine(c4.setRadius(4.0));

            Console.WriteLine("----"); //erotin

            //Luodaan Circle-luokan instanssi c5
            //ja tulostetaan se ToString-methodin avulla.
            //Materiaalin mukaan tämä korvaa c1, mutta tässä se on omana instanssina
            Circle c5 = new Circle(5.0);
            Console.WriteLine(c5.ToString());   // explicit call

            Console.WriteLine("----"); //erotin

            //Luodaan Circle-luokan instanssi c6
            //ja tulostetaan se ToString-methodin avulla.
            //Materiaalin mukaan tämä korvaa c2, mutta tässä se on omana instanssina
            Circle c6 = new Circle(1.2);
            Console.WriteLine(c6.ToString());  // explicit call
            Console.WriteLine(c6);             // println() calls toString() implicitly, same as above
            Console.WriteLine("Operator '+' invokes ToString() too: " + c6);  // '+' invokes toString() too


        }
    }
}
