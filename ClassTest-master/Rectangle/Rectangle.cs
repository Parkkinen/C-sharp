﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rectangle
{
 public   class Rectangle
    {
        private float length;
        private float width;
     

    public Rectangle()
        {
        
            length = 1.0f;
            width = 1.0f;

        }


        public float GetLength()
        { return length; }

        public void Setlength(float length)
        { this.length= length; }

        public float Getwidth()
        { return width; }

        public void Setwidth(float widht)
        { this.width = width ; }

        public double getArea()
        {
            return length * width;
        }
        public double getPerimeter()
        {
            return 2*length + (2*width);
        }
        public string toString()
        {
            return "Rectangle (area=" + getArea() + " perimeter=" + getPerimeter() + ")";
        }

    }

}
