﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Account_Class
{
  public  class Account
    {
        private string id, name;
        private int balance = 0;

    public Account()
        {
            id="ID";
            name="Name";
        }
        public string getid()
        {
            return id;

        }
        public void setid()
        {
            this.id = id;
        }

        public string getname()
        {
            return name;
        
        }
        public int getbalance()
        {
            return balance;

        }
        public int credit(int amount)
        {
            return amount + balance;
        }
                public int debit(int amount)
        {
            if (amount <= balance)
            {
                balance = balance - amount;
            }
            else
            {
               Console.WriteLine("Amount exceeded balance");
            }

            return balance;
        }

        public int transferTo(Account another, int amount)
        {
            if (amount <= balance)
            {
                another.balance += amount;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
            }

            return another.balance;
        }

        public override string ToString()
        {
            return "Account:" + "ID:"+id+" "+"Name:"+name +" " + "balance" + balance;
        }
    }
}
