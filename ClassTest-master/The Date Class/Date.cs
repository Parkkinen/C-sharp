﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Date_Class
{
    public class Date
    {
     private   int day, month, year;


        public Date(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;

        }

        public int Getday()
        {
            return day;
        }

        public int Getmonth()
        {
            return month;
        }

        public int Getyear()
        {
            return year;
        }

        public void setDate(int day,int month,int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public override string ToString()
        {
            return day + " " + month + " " + year+" ";
        }
    }
}
