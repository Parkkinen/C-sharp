﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_InvoiceItem
{
    public class InvoiceItem
    {
        private string id, desc;
        private int gty;
        private double unitPrice;

        public  InvoiceItem(){     
         string id = "Tuote";
        string desc = "Tuotekuvaus";
        int gty = 1;
        double unitPrice = 1;
            }

        public string getID()
        {
            Console.Write("Tuotteen ID: ");
            id = Console.ReadLine();
            return id;
        }
        public string getDesc()
        {
            Console.Write("Tuotteen kuvaus: ");
            desc = Console.ReadLine();
            return desc;
        }

        public int getGty()
        {
            return gty;
        }

        public void setgty(int gty)
        {
         this.gty = gty;
        }

        public double getUnitPrice()
        {
            return unitPrice;
        }

        public void setUnitPrice(double UnitPrice)
        {
            this.unitPrice = UnitPrice;
        }

        public double getTotal()
        {
            return unitPrice * gty;
        }

        public string toString()
        {
            return "Tuote id: " + id + "\n" + "Kuvaus: " + desc + "\n" + "Vero: " + gty + "\n" + "Tuote hinta: " + unitPrice + "\n" + "Yhteensä:" + getTotal();
        }

    }
}
