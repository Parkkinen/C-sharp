﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Time
{
  public  class Time
    {
        private int hour, minute, second;

        public Time(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }

        public int gethour()
        {
            return hour;
        }
        public int getminute()
        {
            return minute;
        }
        public int getsecond()
        {
            return second;
        }
        void sethour(int hour)
        {
            this.hour = hour;
        }
        void setminute(int minute)
        {
            this.minute = minute;
        }
        void setsecond(int second)
        {
            this.second = second;
        }


        public override string ToString()
        {
            return hour+" "+minute+" " + second;
        }
    }

}
